// needs/requires model

const Task = require("../Models/task.js");

/* Controllers and Functions */

// Controller/Function to getAll the tasks in our database
	module.exports.getAll = (request, response) => {

		Task.find({})
		// to capture the result of the find method
		.then(result => {
			return response.send(result);
		})
		// captures the error when the find method is executed
		.catch(error => {
			return response.send(error);
		})
	};

// Add Task on our Database (post)
	module.exports.createTask = (request, response) => {
		const input = request.body;

		// check if existing the user input
		Task.findOne({name: input.name})
		.then(result => {
			if(result !== null){
				return response.send("The task is already existing!")
			} else{
				let newTask = new Task({
					name: input.name
				});

				//for the input to be saved
				newTask.save().then(save => {
					return response.send("The task is successfully added!")
				}).catch(error => {
					return response.send(error)
				})
			}
		}).catch(error => {
			//error for findOne
			return response.send(error)
		})
	}

// Controller that will delete the document that contains the given object
	module.exports.deleteTask = (request, response) => {
		let idToBeDeleted = request.params.id;

		//findbyIdAndRemove - to find the docuemnt that contains the id and then delete the document
		Task.findByIdAndRemove(idToBeDeleted)
		.then(result => {
			return response.send(result);
		}).catch(error => {
			return response.send(error);
		})
	};


/* ---ACTIVITY PROPER--- ayernn */

// Get Specific Task
	module.exports.getTask = (request, response) => {
		let specificID = request.params.id;

		Task.findById(specificID)
		.then((result) => {
			return response.send(result);
		}).catch((error) => {
	      return response.send(error);
	    });
	};

// CHanging the status to "complete"
	module.exports.changeStatus = (request, response) => {
		let idToBeChanged = request.params.id;

		Task.findByIdAndUpdate(
			idToBeChanged, 
			{ status: "complete" },
			{ new: true }

		).then((result) => {
			if(result === null){
				return response.send("No Task Found!")
			} else{
				return response.send(result);
			}
		}).catch((error) => {
			return response.send(error)
		})
	}



/*
Find a document whose
user_id=5eb985d440bd2155e4d788e2 and update it
Updating name field of this user_id to name='Gourav'
var user_id = '5eb985d440bd2155e4d788e2';
	User.findByIdAndUpdate(
		user_id, 
		{ name: 'Gourav' },
		function (err, docs) {
			if (err){
				console.log(err)
			} else{
				console.log("Updated User : ", docs);
			}
	});
*/

