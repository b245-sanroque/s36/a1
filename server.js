// 1st, require all packages needed
	//packages
	const express = require("express");
	const mongoose = require("mongoose");

	//routes
	const taskRoute = require("./Routes/taskRoute.js");

// 2nd, server/app creation
	// check if server is running, nodemon server.js

	const port = 3001;
	// contain express to a variable app
	const app = express();

	//middlewares
	app.use(express.json());
	app.use(express.urlencoded({extended:true}));

	// routing
	//localhost:3001/tasks/get
	app.use("/tasks", taskRoute);

// 3rd, establish database connection
	mongoose.connect("mongodb+srv://admin:admin@batch245-sanroque.9babxkw.mongodb.net/s36-discussion?retryWrites=true&w=majority", {
			useNewUrlParser: true,
			useUnifiedTopology: true
		})

	//check database connection
		let db = mongoose.connection
		
		//error catcher
		db.on("error", console.error.bind(console, "Connection Error!"))
		
		//confirmation of the connection
		db.once("open", ()=> console.log("We are now connected to the cloud!!"));



// will console, if server is running successfully
	app.listen(port, () => console.log(`Server is running at port ${port}!!`));

/*
	SEPARTION OF CONCERNS:
		1. Model should be connected to the Controller
		2. Controller => Routes
		3  Routes => Server/Application

*/