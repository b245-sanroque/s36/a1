const mongoose = require("mongoose");

//Create Task Schema
	const taskSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "Name is required!"]
		},
		status: {
			type: String,
			//not required, so use default
			default: "pending"
		}
	});

//

// for schema to be exportable
module.exports = mongoose.model("Task", taskSchema);	