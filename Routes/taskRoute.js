const express = require("express");
const router = express.Router();

const taskController = require("../Controllers/taskControllers.js");


/* Routes */


/* Route for getTask (all) */
	router.get("/", taskController.getAll);

/* Route for createTask */
	router.post("/addTask", taskController.createTask);

/* Route for deleteTask */
	router.delete("/deleteTask/:id", taskController.deleteTask);



/*-----HAK-TEH-BEH-TEEEH-zartisigz-----*/

/* Route for getTask (specific) */
	router.get("/:id", taskController.getTask);

/* Route for changeStatus */
	router.put("/:id/complete" , taskController.changeStatus);



// needed for router to be exportable
module.exports = router;